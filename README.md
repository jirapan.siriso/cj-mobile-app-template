# cj mobile app template

For mobile application flutter template. development by CJ

## Getting Started

## 1. Setting multiple language support 
- Add code in "pubspec.yaml" 
```dart
flutter_localizations: // add this line
    sdk: flutter // add this line
intl: ^0.17.0 // add this line

flutter:
    uses-material-design: true
    generate: true // add this line
```
    
- Copy file "l10n.yaml" in root project 
- Create directory "lib/l10n" 
- Create file "app_en.arb" and "app_th.arb" in "lib/l10n" 
- Run project 
- Add import code in "lib/main.dart"
```dart
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
```
- Add code in "lib/main.dart"
```dart
localizationsDelegates: const [
    AppLocalizations.delegate,
    GlobalMaterialLocalizations.delegate,
    GlobalWidgetsLocalizations.delegate,
    GlobalCupertinoLocalizations.delegate,
],
supportedLocales: const [
    Locale('en', ''),
    Locale('th', ''),
],
```

## 2. Setting notification
## Android
- Is required the minimum android SDK to 21 (Android 5.0 Lollipop)
- Add code in "AndroidManifest.xml"
```xml
<manifest xmlns:android="http://schemas.android.com/apk/res/android"
    package="com.example.myapp">
   <application>
        ...
        <activity
            android:name=".MainActivity"
            ...
            android:exported="true">
                ...
        </activity>
        ...
    </application>
</manifest>
```
## IOS
- Is required the minimum iOS version to 10

## test