import 'package:cj_app_template/configs/environment.dart';
import 'package:cj_app_template/screens/samples/login/login_screen.dart';
import 'package:cj_app_template/utilities/dialog.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:pretty_dio_logger/pretty_dio_logger.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class AppService {
  String host = applicationMode == dev ? devHost : prodHost;

  Dio dio = Dio(
    BaseOptions(
      receiveDataWhenStatusError: true,
      connectTimeout: 30 * 1000,
      receiveTimeout: 30 * 1000,
    ),
  );

  AppService() {
    dio.interceptors.add(
      PrettyDioLogger(
        requestHeader: applicationMode == dev ? true : false,
        requestBody: applicationMode == dev ? true : false,
        responseBody: applicationMode == dev ? true : false,
        responseHeader: false,
        compact: false,
      ),
    );
  }

  bool statusValidation(int status) {
    return status == 200 ||
        status == 201 ||
        status == 401 ||
        status == 500 ||
        status == 504;
  }

  Future<bool> startLoginScreen(BuildContext context) async {
    // show authen error
    CustomDialog(context: context)
        .showToast(AppLocalizations.of(context)!.responeAuthenticationError);

    // route to login screen
    bool isSuccess = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const LoginScreen(
          mode: loginReAuthenMode,
        ),
      ),
    );

    return isSuccess;
  }
}
