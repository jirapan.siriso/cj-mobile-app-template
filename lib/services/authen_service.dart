import 'package:cj_app_template/constants/interfaces.dart';
import 'package:cj_app_template/services/app_service.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class AuthenService {
  final BuildContext context;
  final OnReLoginCompleted onReLoginCompleted;

  AuthenService({
    required this.context,
    required this.onReLoginCompleted,
  });

  Future<void> startLoginScreen() async {
    // route to login screen
    bool isSuccess = await AppService().startLoginScreen(context);

    // call back
    onReLoginCompleted(isSuccess);
  }

  Future? login(String username, String password) async {
    var url = "${AppService().host}/login";
    Response response = await AppService().dio.post(
      url,
      data: {
        "username": username,
        "password": password,
      },
      options: Options(
        // headers: {HttpHeaders.authorizationHeader: 'Bearer $token'},
        validateStatus: (status) {
          return AppService().statusValidation(status!);
        },
      ),
    );

    if (response.statusCode == 401) {
      await startLoginScreen();
      return null;
    } else {
      return response;
    }
  }

  Future? register(String username, String password) async {
    var url = "${AppService().host}/register";
    Response response = await AppService().dio.post(
      url,
      data: {
        "username": username,
        "password": password,
      },
      options: Options(
        validateStatus: (status) {
          return AppService().statusValidation(status!);
        },
      ),
    );

    if (response.statusCode == 401) {
      await startLoginScreen();
      return null;
    } else {
      return response;
    }
  }
}
