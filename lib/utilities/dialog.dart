import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/constants/interfaces.dart';
import 'package:cj_app_template/widgets/button.dart';
import 'package:cj_app_template/widgets/selector_item.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:image_picker/image_picker.dart';

const iconSize = 50.0;
const buttonWidth = 65.0;

class CustomDialog {
  final BuildContext context;
  final String title;
  final String message;
  final String confirmText;
  final String cancelText;
  final bool showIcon;
  final bool canBack;
  final Widget? child;
  final OnConfirmButtonClicked? onConfirmButtonClicked;
  final OnCancelButtonClicked? onCancelButtonClicked;
  final OnChooseImageCompleted? onChooseImageCompleted;
  final OnSelected? onSelected;

  CustomDialog({
    required this.context,
    this.title = '',
    this.message = '',
    this.cancelText = '',
    this.confirmText = '',
    this.onCancelButtonClicked,
    this.onConfirmButtonClicked,
    this.showIcon = false,
    this.canBack = true,
    this.child,
    this.onChooseImageCompleted,
    this.onSelected,
  });

  void showLoadingDialog() {
    showDialog(
      context: context,
      useSafeArea: true,
      useRootNavigator: true,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Container(
          color: backgroundWhite,
          padding: const EdgeInsets.all(padding),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              Container(
                margin: const EdgeInsets.only(top: 20, bottom: 10),
                child: const SpinKitFadingCircle(
                  color: textWhite,
                  size: 50.0,
                ),
              )
            ],
          ),
        );
      },
    );
  }

  void showConfirmDialog() {
    double width = MediaQuery.of(context).size.width;
    debugPrint(width.toString());
    bool isSmall = false;
    if (width < smallScreenWidth) isSmall = true;

    showDialog(
      barrierDismissible: false,
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: const EdgeInsets.symmetric(horizontal: 14),
          backgroundColor: backgroundWhite,
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
          child: Container(
            width: 350,
            padding: const EdgeInsets.all(23),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                // icon
                Visibility(
                  visible: showIcon,
                  child: SvgPicture.asset(
                    'assets/icons/question.svg',
                    width: iconSize,
                    height: iconSize,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // title
                Text(
                  title,
                  textScaleFactor: 1.0,
                  style: const TextStyle(
                    color: textBlack,
                    fontSize: textLarge,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // description
                child ??
                    Text(
                      message,
                      textScaleFactor: 1.0,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        color: textBlack,
                        fontSize: 16,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                const SizedBox(
                  height: 20,
                ),

                // button
                isSmall
                    ? Column(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          // cancel button
                          CustomButton(
                            onButtonClick: () {
                              dismiss();
                              if (onCancelButtonClicked != null) {
                                onCancelButtonClicked!();
                              }
                            },
                            width: 65,
                            buttonColor: buttonGray,
                            text: cancelText,
                            textColor: textBlack,
                          ),
                          const SizedBox(
                            height: 11,
                          ),
                          // confirm button
                          CustomButton(
                            onButtonClick: () {
                              Navigator.pop(context);
                              if (onConfirmButtonClicked != null) {
                                onConfirmButtonClicked!();
                              }
                            },
                            width: 65,
                            buttonColor: buttonGray,
                            text: confirmText,
                            textColor: textBlack,
                          ),
                        ],
                      )
                    : Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          // cancel button
                          CustomButton(
                            onButtonClick: () {
                              dismiss();
                              if (onCancelButtonClicked != null) {
                                onCancelButtonClicked!();
                              }
                            },
                            width: 65,
                            buttonColor: buttonGray,
                            text: cancelText,
                            textColor: textBlack,
                          ),
                          const SizedBox(
                            width: 11,
                          ),
                          // confirm button
                          CustomButton(
                            onButtonClick: () {
                              Navigator.pop(context);
                              if (onConfirmButtonClicked != null) {
                                onConfirmButtonClicked!();
                              }
                            },
                            width: 65,
                            buttonColor: buttonGray,
                            text: confirmText,
                            textColor: textBlack,
                          ),
                        ],
                      ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showInfoDialog() {
    showDialog(
      context: context,
      barrierDismissible: canBack,
      builder: (BuildContext context) {
        return Dialog(
          backgroundColor: backgroundWhite,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            padding: const EdgeInsets.all(20),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                // icon
                Visibility(
                  visible: showIcon,
                  child: SvgPicture.asset(
                    'assets/icons/info.svg',
                    width: iconSize,
                    height: iconSize,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // title
                Text(
                  title,
                  textScaleFactor: 1.0,
                  style: const TextStyle(
                    color: textBlack,
                    fontSize: textLarge,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // description
                child ??
                    Text(
                      message,
                      textScaleFactor: 1.0,
                      style: const TextStyle(
                        color: textBlack,
                        fontSize: textMeduim,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                const SizedBox(
                  height: 20,
                ),

                // cancel button
                CustomButton(
                  onButtonClick: () {
                    if (onCancelButtonClicked != null) {
                      dismiss();
                      onCancelButtonClicked!();
                    }
                  },
                  width: 65,
                  buttonColor: buttonGray,
                  text: cancelText,
                  textColor: textBlack,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  void showErrorDialog() {
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return Dialog(
          insetPadding: const EdgeInsets.symmetric(horizontal: 14),
          backgroundColor: backgroundWhite,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            padding: const EdgeInsets.all(23),
            child: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                // icon
                Visibility(
                  visible: showIcon,
                  child: SvgPicture.asset(
                    'assets/icons/error.svg',
                    width: iconSize,
                    height: iconSize,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // title
                Text(
                  title,
                  textScaleFactor: 1.0,
                  style: const TextStyle(
                    color: textBlack,
                    fontSize: textLarge,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                const SizedBox(
                  height: 10,
                ),

                // description
                child ??
                    Text(
                      message,
                      textScaleFactor: 1.0,
                      textAlign: TextAlign.center,
                      style: const TextStyle(
                        color: textBlack,
                        fontSize: textMeduim,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                const SizedBox(
                  height: 20,
                ),

                // cancel button
                CustomButton(
                  onButtonClick: () {
                    dismiss();
                    if (onConfirmButtonClicked != null) {
                      onConfirmButtonClicked!();
                    }
                  },
                  width: 65,
                  buttonColor: buttonGray,
                  text: cancelText,
                  textColor: textBlack,
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  showImagePicker() {
    showCupertinoModalPopup(
      context: context,
      builder: (_) => Container(
        height: 220,
        padding: const EdgeInsets.all(padding),
        decoration: const BoxDecoration(
          color: backgroundWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(bottomSheetRound),
            topRight: Radius.circular(bottomSheetRound),
          ),
        ),
        child: StatefulBuilder(
          builder: (context, setState) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: Column(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  // title
                  DefaultTextStyle(
                    style: const TextStyle(
                      color: textBlack,
                      fontSize: textLarge,
                      fontWeight: FontWeight.w500,
                    ),
                    child: Text(
                      AppLocalizations.of(context)!.selectSource,
                      textScaleFactor: 1.0,
                    ),
                  ),

                  const SizedBox(
                    height: padding,
                  ),

                  // choose photo button
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: padding,
                      vertical: padding / 2,
                    ),
                    child: CustomButton(
                      onButtonClick: () {
                        Navigator.pop(context);
                        startChooseImagePicker();
                      },
                      buttonColor: buttonGray,
                      text: AppLocalizations.of(context)!.choosePhoto,
                      textColor: textBlack,
                      fontSize: textMeduim,
                    ),
                  ),

                  // take photo button
                  Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: padding,
                      vertical: padding / 2,
                    ),
                    child: CustomButton(
                      onButtonClick: () {
                        Navigator.pop(context);
                        startTakeImagePicker();
                      },
                      buttonColor: buttonGray,
                      text: AppLocalizations.of(context)!.takePhoto,
                      textColor: textBlack,
                      fontSize: textMeduim,
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  showToast(String message) {
    Fluttertoast.showToast(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.CENTER,
        timeInSecForIosWeb: 1,
        backgroundColor: backgroundDark,
        textColor: textWhite,
        fontSize: textMeduim);
  }

  Future<void> dismiss() async {
    if (Navigator.canPop(context)) Navigator.pop(context);
  }

  startChooseImagePicker() async {
    final ImagePicker imagePicker = ImagePicker();

    final XFile? image =
        await imagePicker.pickImage(source: ImageSource.gallery);

    if (onChooseImageCompleted != null) {
      onChooseImageCompleted!(image);
    }
  }

  startTakeImagePicker() async {
    final ImagePicker imagePicker = ImagePicker();

    final XFile? image =
        await imagePicker.pickImage(source: ImageSource.camera);

    if (onChooseImageCompleted != null) {
      onChooseImageCompleted!(image);
    }
  }

  showSelectorPicker(List<String> items, bool enableSearch) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    TextEditingController searchController = TextEditingController();
    showCupertinoModalPopup(
      context: context,
      builder: (_) => Container(
        height: height * 0.70,
        padding: const EdgeInsets.all(padding),
        decoration: const BoxDecoration(
          color: backgroundWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(bottomSheetRound),
            topRight: Radius.circular(bottomSheetRound),
          ),
        ),
        child: StatefulBuilder(
          builder: (context, setState) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: SizedBox(
                width: width,
                height: height * 0.70,
                child: Stack(
                  children: [
                    // title
                    Align(
                      alignment: Alignment.topCenter,
                      child: Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: DefaultTextStyle(
                          style: const TextStyle(
                            color: textBlack,
                            fontSize: textLarge,
                            fontWeight: FontWeight.w500,
                          ),
                          child: Text(
                            title,
                            textScaleFactor: 1.0,
                            maxLines: 1,
                          ),
                        ),
                      ),
                    ),

                    // search
                    Visibility(
                      visible: enableSearch,
                      child: Material(
                        color: backgroundWhite,
                        child: Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: TextFormField(
                            maxLines: 1,
                            cursorColor: textBlack,
                            style: const TextStyle(
                              fontSize: textMeduim,
                              fontWeight: FontWeight.w400,
                              color: textBlack,
                            ),
                            controller: searchController,
                            decoration: InputDecoration(
                              contentPadding: const EdgeInsets.all(padding),
                              hintStyle: const TextStyle(
                                color: textGray,
                              ),
                              filled: false,
                              fillColor: backgroundWhite,
                              hintText:
                                  AppLocalizations.of(context)!.searchHere,
                              border: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(textFieldRound),
                                borderSide: const BorderSide(
                                  color: textGray,
                                  width: 2.0,
                                ),
                              ),
                              enabledBorder: OutlineInputBorder(
                                borderRadius:
                                    BorderRadius.circular(textFieldRound),
                                borderSide: const BorderSide(
                                  color: textGray,
                                  width: 2.0,
                                ),
                              ),
                              focusColor: textBlue,
                            ),
                          ),
                        ),
                      ),
                    ),

                    // items
                    Padding(
                      padding: const EdgeInsets.only(top: 70),
                      child: ListView.builder(
                        itemCount: items.length,
                        padding: const EdgeInsets.all(0),
                        itemBuilder: (BuildContext context, int index) {
                          return SelectorItem(
                            text: items[index],
                            onClicked: () {
                              // close selector
                              Navigator.pop(context);

                              // call back
                              if (onSelected != null) {
                                onSelected!(index);
                              }
                            },
                          );
                        },
                      ),
                    ),
                  ],
                ),
              ),
            );
          },
        ),
      ),
    );
  }
}
