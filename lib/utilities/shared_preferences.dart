import 'dart:convert';
import 'dart:async';
import 'package:cj_app_template/models/session.dart';
import 'package:shared_preferences/shared_preferences.dart';

class CustomSharedPreferences {
  final String session = "SESSION_KEY";

  static CustomSharedPreferences instant() {
    return CustomSharedPreferences();
  }

  // clear
  Future<void> clearSession() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove(session);
  }

  // get method
  Future<Session?> getSession() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? json = prefs.getString(session);
    if (json != null && json.isNotEmpty) {
      Session session = Session.fromJson(jsonDecode(json));
      return session;
    } else {
      return null;
    }
  }

  // set method
  Future setSession(Session sessionObj) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String json = jsonEncode(sessionObj);
    prefs.setString(session, json);
  }
}
