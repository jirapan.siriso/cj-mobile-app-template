import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/constants/interfaces.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

const textSize = 18.0;
const fontWeight = FontWeight.w500;
const textColor = textBlack;

class CustomDateTimePicker {
  OnDateChange? onDateChange;
  OnTimeChange? onTimeChange;
  BuildContext context;
  DateTime? defaultDateTime;
  int? minYear;
  int? maxYear;
  bool? enableCurrentDate;
  TimeOfDay? defaultTimeOfDay;
  bool dateVisible;
  bool monthVisible;
  bool yearVisible;

  CustomDateTimePicker({
    required this.context,
    this.defaultTimeOfDay,
    this.onDateChange,
    this.defaultDateTime,
    this.minYear = 1950,
    this.maxYear,
    this.enableCurrentDate = true,
    this.onTimeChange,
    this.dateVisible = true,
    this.monthVisible = true,
    this.yearVisible = true,
  });

  List<String>? months;
  List<int>? years;
  List<int>? hours;
  List<int>? minutes;
  DateTime? dateTime;
  TimeOfDay? time;
  int? dayIndex;
  int? monthIndex;
  int? yearIndex;
  int? hourIndex;
  int? minuteIndex;
  int? dayCount;
  int? increase;
  FixedExtentScrollController? dayScrollController;
  FixedExtentScrollController? monthScrollController;
  FixedExtentScrollController? yearScrollController;
  FixedExtentScrollController? hourScrollController;
  FixedExtentScrollController? minuteScrollController;

  // Date picker

  _initDatePickerData() {
    DateTime current = DateTime.now();

    if (dateTime == null) {
      if (defaultDateTime != null) {
        dateTime = defaultDateTime!;
      } else {
        dateTime = DateTime.now();
      }
    }

    // init months
    _setMonths();

    // init year
    // ignore: prefer_conditional_assignment
    if (maxYear == null) {
      maxYear = DateTime.now().year;
    }
    years = [];
    increase = 0;
    yearIndex = 0;
    increase = 543;
    int newMaxYear = maxYear!;
    if (enableCurrentDate!) newMaxYear = current.year;
    for (int i = 0; i <= newMaxYear - minYear!; i++) {
      years!.add(minYear! + i + increase!);
      if (minYear! + i == dateTime!.year) yearIndex = i;
    }

    // init day

    dayCount = DateUtils.getDaysInMonth(dateTime!.year, dateTime!.month);
    if (enableCurrentDate!) {
      if (dateTime!.year == current.year && dateTime!.month == current.month) {
        dayCount = dateTime!.day;
      }
    }

    dayIndex = dateTime!.day - 1;
    monthIndex = dateTime!.month - 1;

    // init controller
    if (dayScrollController == null) {
      dayScrollController = FixedExtentScrollController(initialItem: dayIndex!);
    } else {
      _daySelected(dayIndex!);
    }
    if (monthScrollController == null) {
      monthScrollController =
          FixedExtentScrollController(initialItem: monthIndex!);
    } else {
      _monthSelected(monthIndex!);
    }

    if (yearScrollController == null) {
      yearScrollController =
          FixedExtentScrollController(initialItem: yearIndex!);
    } else {
      _yearSelected(yearIndex!);
    }

    // call back default date time
    if (defaultDateTime != null && onDateChange != null) {
      onDateChange!(defaultDateTime!);
    } else if (onDateChange != null) {
      onDateChange!(DateTime.now());
    }
  }

  _setMonths() {
    // check small screen
    double width = MediaQuery.of(context).size.width;
    bool isSmall = false;
    if (width < smallScreenWidth) isSmall = true;

    DateTime current = DateTime.now();
    months = [
      isSmall
          ? AppLocalizations.of(context)!.jan
          : AppLocalizations.of(context)!.january,
      isSmall
          ? AppLocalizations.of(context)!.feb
          : AppLocalizations.of(context)!.february,
      isSmall
          ? AppLocalizations.of(context)!.mar
          : AppLocalizations.of(context)!.march,
      isSmall
          ? AppLocalizations.of(context)!.apr
          : AppLocalizations.of(context)!.april,
      isSmall
          ? AppLocalizations.of(context)!.may
          : AppLocalizations.of(context)!.may,
      isSmall
          ? AppLocalizations.of(context)!.jun
          : AppLocalizations.of(context)!.june,
      isSmall
          ? AppLocalizations.of(context)!.jul
          : AppLocalizations.of(context)!.july,
      isSmall
          ? AppLocalizations.of(context)!.aug
          : AppLocalizations.of(context)!.august,
      isSmall
          ? AppLocalizations.of(context)!.sep
          : AppLocalizations.of(context)!.september,
      isSmall
          ? AppLocalizations.of(context)!.oct
          : AppLocalizations.of(context)!.october,
      isSmall
          ? AppLocalizations.of(context)!.nov
          : AppLocalizations.of(context)!.november,
      isSmall
          ? AppLocalizations.of(context)!.dec
          : AppLocalizations.of(context)!.december
    ];
    if (enableCurrentDate!) {
      if (dateTime!.year == current.year) {
        int start = current.month;
        for (int i = 11; i >= start; i--) {
          months!.removeAt(i);
        }
      }
    }
  }

  _yearSelected(int position) {
    yearScrollController!.animateToItem(
      position,
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  _monthSelected(int position) {
    monthScrollController!.animateToItem(
      position,
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  _daySelected(int position) {
    dayScrollController!.animateToItem(
      position,
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  void showDatePickerDialog() {
    _initDatePickerData();

    // check small screen
    double width = MediaQuery.of(context).size.width;
    bool isSmall = false;
    if (width < smallScreenWidth) isSmall = true;

    showCupertinoModalPopup(
      context: context,
      builder: (_) => Container(
        height: 300,
        padding: const EdgeInsets.all(bottomSheetRound),
        decoration: const BoxDecoration(
          color: backgroundWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(bottomSheetRound),
            topRight: Radius.circular(bottomSheetRound),
          ),
        ),
        child: StatefulBuilder(
          builder: (context, setState) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // day
                  Visibility(
                    visible: dateVisible,
                    child: SizedBox(
                      height: 300,
                      width: 70,
                      child: CupertinoPicker.builder(
                        // key: _pickerKey,
                        squeeze: 1.5,
                        diameterRatio: 1,
                        useMagnifier: true,
                        scrollController: dayScrollController,
                        itemExtent: 33.0,
                        backgroundColor: Colors.white,
                        onSelectedItemChanged: (int index) {
                          dayIndex = index;

                          // set date
                          dateTime = DateTime(
                            years![yearIndex!] - increase!,
                            dateTime!.month,
                            dayIndex! + 1,
                          );

                          // call back
                          if (onDateChange != null) onDateChange!(dateTime!);
                        },
                        childCount: dayCount,
                        itemBuilder: (context, index) {
                          return Center(
                            child: Text(
                              '${index + 1}',
                              textScaleFactor: 1.0,
                              style: const TextStyle(
                                color: textColor,
                                fontWeight: fontWeight,
                                fontSize: textSize,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  // month
                  Visibility(
                    visible: monthVisible,
                    child: SizedBox(
                      height: 300,
                      width: isSmall ? 70 : 120,
                      child: CupertinoPicker.builder(
                        // key: _pickerKey,
                        squeeze: 1.5,
                        diameterRatio: 1,
                        useMagnifier: true,
                        scrollController: monthScrollController,
                        itemExtent: 33.0,
                        backgroundColor: Colors.white,
                        onSelectedItemChanged: (int index) {
                          monthIndex = index;

                          // set day
                          dayIndex = 0;
                          DateTime current = DateTime.now();
                          if (years![yearIndex!] - increase! == current.year &&
                              index == current.month - 1) {
                            dayIndex = current.day - 1;
                          }

                          // set date
                          dateTime = DateTime(
                            years![yearIndex!] - increase!,
                            index + 1,
                            dayIndex! + 1,
                          );

                          // call back
                          if (onDateChange != null) onDateChange!(dateTime!);

                          // update widget
                          setState(() {
                            dayCount = DateUtils.getDaysInMonth(
                                dateTime!.year, dateTime!.month);
                            if (dateTime!.year == current.year &&
                                dateTime!.month == current.month) {
                              dayCount = dateTime!.day;
                            }
                          });

                          // set day controller scroll to day position selected
                          _daySelected(dayIndex!);
                        },
                        childCount: months!.length,
                        itemBuilder: (context, index) {
                          return Center(
                            child: Text(
                              months![index],
                              textScaleFactor: 1.0,
                              style: const TextStyle(
                                color: textColor,
                                fontWeight: fontWeight,
                                fontSize: textSize,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                  // year
                  Visibility(
                    visible: yearVisible,
                    child: SizedBox(
                      height: 300,
                      width: 90,
                      child: CupertinoPicker.builder(
                        // key: _pickerKey,
                        squeeze: 1.5,
                        diameterRatio: 1,

                        useMagnifier: true,
                        scrollController: yearScrollController,
                        itemExtent: 33.0,
                        backgroundColor: Colors.white,
                        onSelectedItemChanged: (int index) {
                          yearIndex = index;

                          // set day and month
                          dayIndex = 0;
                          monthIndex = 0;
                          DateTime current = DateTime.now();
                          if (years![yearIndex!] - increase! == current.year) {
                            dayIndex = current.day - 1;
                            monthIndex = current.month - 1;
                          }

                          // set date
                          dateTime = DateTime(
                            years![index] - increase!,
                            monthIndex! + 1,
                            dayIndex! + 1,
                          );

                          // init new months in year
                          _setMonths();

                          // call back
                          if (onDateChange != null) onDateChange!(dateTime!);

                          // update widget
                          setState(() {
                            dayCount = DateUtils.getDaysInMonth(
                                dateTime!.year, dateTime!.month);
                            if (dateTime!.year == current.year &&
                                dateTime!.month == current.month) {
                              dayCount = dateTime!.day;
                            }
                          });

                          // set month controller scroll to month position selected
                          _monthSelected(monthIndex!);

                          // set day controller scroll to day position selected
                          _daySelected(dayIndex!);
                        },
                        childCount: years!.length,
                        itemBuilder: (context, index) {
                          return Center(
                            child: Text(
                              '${years![index]}',
                              textScaleFactor: 1.0,
                              style: const TextStyle(
                                color: textColor,
                                fontWeight: fontWeight,
                                fontSize: textSize,
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }

  // Time picker

  _initTimePickerData() {
    if (time == null) {
      if (defaultTimeOfDay != null) {
        time = defaultTimeOfDay!;
      } else {
        time = TimeOfDay.now();
      }
    }

    // set hours
    hours = [];
    for (int hour = 0; hour < 24; hour++) {
      hours!.add(hour);
      if (hour == time!.hour) hourIndex = hour;
    }

    // set hours
    minutes = [];
    for (int minute = 0; minute < 60; minute++) {
      minutes!.add(minute);
      if (minute == time!.minute) minuteIndex = minute;
    }

    // init controller
    if (hourScrollController == null) {
      hourScrollController =
          FixedExtentScrollController(initialItem: hourIndex!);
    } else {
      _hourSelected(hourIndex!);
    }
    if (minuteScrollController == null) {
      minuteScrollController =
          FixedExtentScrollController(initialItem: minuteIndex!);
    } else {
      _minuteSelected(minuteIndex!);
    }

    // call back default date time
    if (defaultTimeOfDay != null && onTimeChange != null) {
      onTimeChange!(time!);
    } else if (onTimeChange != null) {
      onTimeChange!(TimeOfDay.now());
    }
  }

  _hourSelected(int position) {
    hourScrollController!.animateToItem(
      position,
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  _minuteSelected(int position) {
    minuteScrollController!.animateToItem(
      position,
      duration: const Duration(milliseconds: 300),
      curve: Curves.ease,
    );
  }

  void showTimePickerDialog() {
    _initTimePickerData();

    showCupertinoModalPopup(
      context: context,
      builder: (_) => Container(
        height: 300,
        padding: const EdgeInsets.all(bottomSheetRound),
        decoration: const BoxDecoration(
          color: backgroundWhite,
          borderRadius: BorderRadius.only(
            topLeft: Radius.circular(bottomSheetRound),
            topRight: Radius.circular(bottomSheetRound),
          ),
        ),
        child: StatefulBuilder(
          builder: (context, setState) {
            return MediaQuery(
              data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  // hours
                  SizedBox(
                    height: 300,
                    width: 70,
                    child: CupertinoPicker.builder(
                      // key: _pickerKey,
                      squeeze: 1.5,
                      diameterRatio: 1,
                      useMagnifier: true,
                      scrollController: hourScrollController,
                      itemExtent: 33.0,
                      backgroundColor: Colors.white,
                      onSelectedItemChanged: (int index) {
                        hourIndex = index;

                        // set date
                        time =
                            TimeOfDay(hour: hourIndex!, minute: time!.minute);

                        // call back
                        if (onTimeChange != null) onTimeChange!(time!);
                      },
                      childCount: hours!.length,
                      itemBuilder: (context, index) {
                        return Center(
                          child: Text(
                            hours![index] < 10
                                ? '0${hours![index]}'
                                : '${hours![index]}',
                            textScaleFactor: 1.0,
                            style: const TextStyle(
                              color: textColor,
                              fontWeight: fontWeight,
                              fontSize: textSize,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                  const DefaultTextStyle(
                    style: TextStyle(
                      color: textBlack,
                      fontSize: textLarge,
                      fontWeight: FontWeight.w500,
                    ),
                    child: Text(
                      ":",
                      textScaleFactor: 1.0,
                    ),
                  ),
                  // minutes
                  SizedBox(
                    height: 300,
                    width: 70,
                    child: CupertinoPicker.builder(
                      // key: _pickerKey,
                      squeeze: 1.5,
                      diameterRatio: 1,
                      useMagnifier: true,
                      scrollController: minuteScrollController,
                      itemExtent: 33.0,
                      backgroundColor: Colors.white,
                      onSelectedItemChanged: (int index) {
                        minuteIndex = index;

                        // set date
                        time =
                            TimeOfDay(hour: time!.hour, minute: minuteIndex!);

                        // call back
                        if (onTimeChange != null) onTimeChange!(time!);
                      },
                      childCount: minutes!.length,
                      itemBuilder: (context, index) {
                        return Center(
                          child: Text(
                            minutes![index] < 10
                                ? '0${minutes![index]}'
                                : '${minutes![index]}',
                            textScaleFactor: 1.0,
                            style: const TextStyle(
                              color: textColor,
                              fontWeight: fontWeight,
                              fontSize: textSize,
                            ),
                          ),
                        );
                      },
                    ),
                  ),
                ],
              ),
            );
          },
        ),
      ),
    );
  }
}
