import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:intl/intl.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

const be = 543;

class Tools {
  final BuildContext context;
  List<String> monthNames = [];
  List<String> monthShortNames = [];

  Tools({required this.context});

  static Tools instant({required BuildContext context}) {
    return Tools(context: context);
  }

  initMonth() {
    monthNames = [
      AppLocalizations.of(context)!.january,
      AppLocalizations.of(context)!.february,
      AppLocalizations.of(context)!.march,
      AppLocalizations.of(context)!.april,
      AppLocalizations.of(context)!.may,
      AppLocalizations.of(context)!.june,
      AppLocalizations.of(context)!.july,
      AppLocalizations.of(context)!.august,
      AppLocalizations.of(context)!.september,
      AppLocalizations.of(context)!.october,
      AppLocalizations.of(context)!.november,
      AppLocalizations.of(context)!.december
    ];
    monthShortNames = [
      AppLocalizations.of(context)!.jan,
      AppLocalizations.of(context)!.feb,
      AppLocalizations.of(context)!.mar,
      AppLocalizations.of(context)!.apr,
      AppLocalizations.of(context)!.may,
      AppLocalizations.of(context)!.jun,
      AppLocalizations.of(context)!.jul,
      AppLocalizations.of(context)!.aug,
      AppLocalizations.of(context)!.sep,
      AppLocalizations.of(context)!.oct,
      AppLocalizations.of(context)!.nov,
      AppLocalizations.of(context)!.dec
    ];
  }

  void setPortraitScreen() {
    SystemChrome.setPreferredOrientations(
      [
        DeviceOrientation.portraitUp,
        DeviceOrientation.portraitDown,
      ],
    );
  }

  String? getDateFormat(DateTime dateTime, String format) {
    return DateFormat(format).format(dateTime);
  }

  String? getDateBEForDisplay(DateTime dateTime, bool isLongFormat) {
    initMonth();
    int year = dateTime.year + be;
    int month = dateTime.month;
    int day = dateTime.day;
    // int dayOfWeek = dateTime.weekday;
    return '$day ${isLongFormat ? monthNames[month - 1] : monthShortNames[month - 1]} $year';
  }

  String? getDateTimeBEForDisplay(DateTime dateTime, bool isLongFormat) {
    initMonth();
    int year = dateTime.year + be;
    int month = dateTime.month;
    int day = dateTime.day;
    int hour = dateTime.hour;
    int minute = dateTime.minute;
    return '$day ${isLongFormat ? monthNames[month - 1] : monthShortNames[month - 1]} $year $hour:$minute';
  }

  String? getCurrentDateRequest() {
    return DateFormat("yyyy-MM-dd").format(DateTime.now());
  }

  String? getDateRequest(DateTime dateTime) {
    return DateFormat("yyyy-MM-dd").format(dateTime);
  }
}
