import 'package:cj_app_template/models/screen_result.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

typedef OnButtonClick = Function();
typedef OnClicked = Function();
typedef OnScreenResult = Function(ScreenResult? value);
typedef OnSubmit = Function(String username, String password);
typedef OnReLoginCompleted = Function(bool);
typedef OnDateChange = Function(DateTime dateTime);
typedef OnTimeChange = Function(TimeOfDay timeOfDay);
typedef OnConfirmButtonClicked = Function();
typedef OnCancelButtonClicked = Function();
typedef OnChooseImageCompleted = Function(XFile? image);
typedef OnSelected = Function(int index);
typedef OnTab = Function();
