import 'package:flutter/material.dart';

// background
const backgroundWhite = Color(0xFFFFFFFF);
const backgroundGray = Color(0xFFAEAEAE);
const backgroundBlack = Color(0xFF000000);
const backgroundDark = Color(0xFF2D2D2D);
const backgroundTransparent = Color(0x00FFFFFF);
const backgroundTransparentWhite75 = Color(0xC2FFFFFF);
const backgroundTransparentWhite50 = Color(0x86FFFFFF);
const backgroundTransparentWhite25 = Color(0x3CFFFFFF);
const backgroundTransparentBlack75 = Color(0xBE000000);
const backgroundTransparentBlack50 = Color(0x86000000);
const backgroundTransparentBlack25 = Color(0x38000000);

// text
const textWhite = Color(0xFFFFFFFF);
const textGray = Color(0xFF626262);
const textGreen = Color(0xFF317B27);
const textRed = Color(0xFF992323);
const textOrange = Color(0xFFA85D24);
const textBlue = Color(0xFF1347B0);
const textBlack = Color(0xFF000000);
const textYellow = Color(0xFFFDD54C);

// button
const buttonGray = Color(0xFFECECEC);
const buttonGreen = Color(0xFF317B27);
const buttonRed = Color(0xFF992323);
const buttonYellow = Color(0xFFD5AB23);
const buttonWhite = Color(0xFFFFFFFF);

// text field
const textFeildBorderGray = Color(0xFF626262);
const textFeildBorderBlue = Color(0xFF1347B0);
const textFeildBorderRed = Color(0xFF992323);
