// layout
const padding = 15.0;
const smallScreenWidth = 320.0;

// text
const textLarge = 20.0;
const textMeduim = 16.0;
const textSmall = 12.0;

// app bar
const appBarHeight = 60.0;

// button
const buttonHeight = 45.0;
const buttonRound = 5.0;

// text field
const textFieldRound = 5.0;

// bottom sheet picker
const bottomSheetRound = 10.0;
