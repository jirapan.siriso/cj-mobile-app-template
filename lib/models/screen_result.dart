class ScreenResult {
  ScreenResult({
    this.status = false,
  });

  bool status;

  factory ScreenResult.fromJson(Map<String, dynamic> json) => ScreenResult(
        status: json["ok"],
      );

  Map<String, dynamic> toJson() => {
        "ok": status,
      };
}
