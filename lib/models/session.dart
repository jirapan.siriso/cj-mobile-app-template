import 'package:cj_app_template/models/user.dart';

class Session {
  Session({
    this.ok,
    this.accessToken,
    this.user,
  });

  bool? ok;
  String? accessToken;
  User? user;

  factory Session.fromJson(Map<String, dynamic> json) => Session(
        ok: json["ok"],
        accessToken: json["access_token"],
        user: json["user"] != null ? User.fromJson(json["user"]) : null,
      );

  Map<String, dynamic> toJson() => {
        "ok": ok,
        "access_token": accessToken,
        "user": user != null ? user!.toJson() : null,
      };
}
