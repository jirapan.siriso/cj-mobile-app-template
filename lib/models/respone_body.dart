import 'package:cj_app_template/models/session.dart';

class ResponeBody {
  ResponeBody({
    this.ok,
    this.session,
    // this.firms,
  });

  bool? ok;
  Session? session;
  // List<Session>? firms;

  factory ResponeBody.fromJson(Map<String, dynamic> json) => ResponeBody(
        ok: json["ok"],
        session:
            json["session"] == null ? null : Session.fromJson(json["session"]),
        // firms: json["firms"] == null
        //     ? null
        //     : List<Session>.from(
        //         json["firms"].map((object) => Session.fromJson(object))),
      );

  Map<String, dynamic> toJson() => {
        "ok": ok,
        "session": session == null ? null : session!.toJson(),
      };
}
