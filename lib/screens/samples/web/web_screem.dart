import 'dart:io';
import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:webview_flutter/webview_flutter.dart';

class WebScreen extends StatefulWidget {
  const WebScreen({Key? key}) : super(key: key);

  @override
  State<WebScreen> createState() => _WebScreenState();
}

class _WebScreenState extends State<WebScreen> {
  int progress = 0;
  bool progressVisible = false;
  String url = 'https://flutter.dev/?gclid=Cj0KCQjw8amWBhCYARIsADqZJoXfsuxaj72rOe4ZJ_CBYrjmlz_ae8Nf9Z-4apjOyHBeb-lJcI91b8YaAvL8EALw_wcB&gclsrc=aw.ds';
  WebViewController? controller;

  @override
  void initState() {
    super.initState();
    // enable virtual display.
    if (Platform.isAndroid) WebView.platform = AndroidWebView();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showBackButton: true,
        backButtonClick: () {
          Navigator.pop(context);
        },
        text: AppLocalizations.of(context)!.web,
        backgroundColor: backgroundDark,
      ),
      body: body(),
    );
  }

  Widget body() {
    return Stack(
      children: [
        // webview
        WebView(
          backgroundColor: backgroundWhite,
          initialUrl: url,
          javascriptMode: JavascriptMode.unrestricted,
          gestureNavigationEnabled: true,
          navigationDelegate: (NavigationRequest request) {
            if (request.url.startsWith('https://www.youtube.com/')) {
              debugPrint('blocking navigation to $request}');
              return NavigationDecision.prevent;
            }
            debugPrint('allowing navigation to $request');
            return NavigationDecision.navigate;
          },
          onProgress: (value) {
            debugPrint('processing : $value');
            if (!mounted) return;
            setState(() {
              if (value > 0) {
                progressVisible = true;
              }
              if (value > progress) {
                progress = value;
              }
            });
          },
          onPageStarted: (url) {},
          onPageFinished: (url) {
            Future.delayed(const Duration(milliseconds: 500), () {
              if (!mounted) return;
              setState(() {
                progressVisible = false;
              });
            });
          },
          onWebViewCreated: (c) {
            controller = c;
          },
        ),

        // loading
        linearLoading(),
      ],
    );
  }

  Widget linearLoading() {
    double width = MediaQuery.of(context).size.width;
    return Visibility(
      child: LinearPercentIndicator(
        padding: const EdgeInsets.all(0),
        width: width,
        lineHeight: 3,
        percent: progress.toDouble() / 100,
        progressColor: textBlue,
        backgroundColor: textWhite,
        barRadius: const Radius.circular(0),
      ),
      visible: progressVisible,
    );
  }
}
