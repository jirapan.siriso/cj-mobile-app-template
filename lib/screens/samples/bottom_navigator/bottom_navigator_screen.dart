import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/screens/samples/pager/components/setting_page.dart';
import 'package:cj_app_template/screens/samples/pager/components/home_page.dart';
import 'package:cj_app_template/screens/samples/pager/components/location_page.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:convex_bottom_bar/convex_bottom_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class BottomNavigatorScreen extends StatefulWidget {
  const BottomNavigatorScreen({Key? key}) : super(key: key);

  @override
  State<BottomNavigatorScreen> createState() => _BottomNavigatorScreenState();
}

class _BottomNavigatorScreenState extends State<BottomNavigatorScreen>
    with SingleTickerProviderStateMixin {
  PageController? pageController;
  TabController? tabController;
  int currentPosition = 0;

  @override
  void initState() {
    super.initState();
    pageController = PageController(
      initialPage: currentPosition,
      // viewportFraction: 0.8,
    );
    tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    pageController!.dispose();
    tabController!.dispose();
    super.dispose();
  }

  popScreen() {
    // set screen result
    ScreenResult result = ScreenResult(
      status: false,
    );
    // pop and send result
    Navigator.pop(context, result);
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showBackButton: true,
        backButtonClick: () {
          Navigator.pop(context);
        },
        text: AppLocalizations.of(context)!.bottomMenu,
        backgroundColor: backgroundDark,
      ),
      body: body(),
      // bottomNavigationBar: bottomNavigatorBar(),
      bottomNavigationBar: bottomAppBar(),
      // bottomNavigationBar: convexAppBar(),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      floatingActionButton: Padding(
        padding: const EdgeInsets.all(8.0),
        child: FloatingActionButton(
          backgroundColor: currentPosition == 1 ? textBlack : textWhite,
          child: Icon(
            Icons.place,
            color: currentPosition == 1 ? textWhite : textGray,
          ),
          onPressed: () {
            if (!mounted) return;
            setState(() {
              currentPosition = 1;
            });
            pageController!.animateToPage(
              currentPosition,
              duration: const Duration(milliseconds: 500),
              curve: Curves.ease,
            );
          },
        ),
      ),
    );
  }

  Widget appBar() {
    return Container(
      height: appBarHeight,
      color: backgroundDark,
      alignment: Alignment.centerLeft,
      child: Text(
        AppLocalizations.of(context)!.bottomMenu,
        textScaleFactor: 1.0,
        style: const TextStyle(
          fontSize: 20,
          color: textWhite,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  Widget body() {
    return PageView(
      controller: pageController,
      physics: const NeverScrollableScrollPhysics(),
      scrollDirection: Axis.horizontal,
      onPageChanged: (value) {
        currentPosition = value;
        tabController!.animateTo(
          value,
          duration: const Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      },
      children: const [
        HomePage(),
        LocationPage(),
        SettingPage(),
      ],
    );
  }

  BottomNavigationBar bottomNavigatorBar() {
    return BottomNavigationBar(
      items: const <BottomNavigationBarItem>[
        BottomNavigationBarItem(
          icon: Icon(Icons.home),
          label: 'Home',
          backgroundColor: Colors.red,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.pin),
          label: 'Location',
          backgroundColor: Colors.green,
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings_outlined),
          label: 'Setting',
          backgroundColor: Colors.purple,
        ),
      ],
      currentIndex: currentPosition,
      unselectedItemColor: textGray,
      selectedItemColor: textBlack,
      onTap: (value) {
        if (!mounted) return;
        setState(() {
          currentPosition = value;
        });
        pageController!.animateToPage(
          value,
          duration: const Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      },
    );
  }

  BottomAppBar bottomAppBar() {
    return BottomAppBar(
      shape: const CircularNotchedRectangle(),
      notchMargin: 8.0,
      clipBehavior: Clip.antiAlias,
      child: SizedBox(
        height: appBarHeight,
        child: BottomNavigationBar(
          currentIndex: currentPosition,
          backgroundColor: backgroundWhite,
          selectedItemColor: textBlack,
          unselectedItemColor: textGray,
          elevation: 0.0,
          type: BottomNavigationBarType.fixed,
          onTap: (value) {
            if (!mounted) return;
            setState(() {
              currentPosition = value;
            });
            pageController!.animateToPage(
              value,
              duration: const Duration(milliseconds: 500),
              curve: Curves.ease,
            );
          },
          items: const [
            BottomNavigationBarItem(
              icon: Icon(Icons.home),
              label: 'Home',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.place),
              label: '',
            ),
            BottomNavigationBarItem(
              icon: Icon(Icons.settings_outlined),
              label: 'Setting',
            )
          ],
        ),
      ),
    );
  }

  ConvexAppBar convexAppBar() {
    return ConvexAppBar(
      height: appBarHeight,
      style: TabStyle.textIn,
      items: const [
        TabItem(
          icon: Icons.home,
          title: 'Home',
        ),
        TabItem(
          icon: Icons.place,
          title: 'Location',
        ),
        TabItem(
          icon: Icons.settings_outlined,
          title: 'Setting',
        ),
      ],
      initialActiveIndex: currentPosition,
      backgroundColor: backgroundWhite,
      color: textGray,
      activeColor: textBlack,
      top: -20,
      onTap: (value) {
        pageController!.animateToPage(
          value,
          duration: const Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      },
    );
  }
}
