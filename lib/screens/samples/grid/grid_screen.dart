import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:cj_app_template/widgets/shimmer_grid.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class GridScreen extends StatefulWidget {
  const GridScreen({Key? key}) : super(key: key);

  @override
  State<GridScreen> createState() => _GridScreenState();
}

class _GridScreenState extends State<GridScreen> {
  bool isLoading = false;
  @override
  void initState() {
    super.initState();

    if (!mounted) return;
    setState(() {
      isLoading = true;
    });
    Future.delayed(const Duration(milliseconds: 2000), () {
      if (!mounted) return;
      setState(() {
        isLoading = false;
      });
    });
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showBackButton: true,
        backButtonClick: () {
          Navigator.pop(context);
        },
        text: AppLocalizations.of(context)!.grid,
        backgroundColor: backgroundDark,
      ),
      body: isLoading
          ? const ShimmerGrid(
              column: 3,
            )
          : body(),
    );
  }

  Widget body() {
    return GridView.builder(
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
      ),
      padding: const EdgeInsets.all(padding / 2),
      itemCount: 300,
      itemBuilder: (BuildContext context, int index) {
        return Card(
          color: buttonGray,
          child: Center(
            child: Text(
              '$index',
              textScaleFactor: 1.0,
              style: const TextStyle(
                color: textBlack,
                fontSize: textLarge,
                fontWeight: FontWeight.w700,
              ),
            ),
          ),
        );
      },
    );
  }
}
