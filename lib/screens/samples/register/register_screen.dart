import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/screens/samples/register/components/register_form.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:responsive_builder/responsive_builder.dart';

class RegisterScreen extends StatefulWidget {
  const RegisterScreen({Key? key}) : super(key: key);

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  @override
  void initState() {
    super.initState();
  }

  popScreen() {
    // set screen result
    ScreenResult result = ScreenResult(
      status: false,
    );
    // pop and send result
    Navigator.pop(context, result);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: Scaffold(
        appBar: CustomAppBar(
          showBackButton: true,
          backButtonClick: () {
            Navigator.pop(context);
          },
          text: AppLocalizations.of(context)!.register,
          backgroundColor: backgroundDark,
        ),
        body: SingleChildScrollView(
          child: body(),
        ),
      ),
    );
  }

  Widget body() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ResponsiveBuilder(builder: (context, size) {
      if (size.deviceScreenType == DeviceScreenType.mobile ||
          size.deviceScreenType == DeviceScreenType.watch) {
        return Container(
          alignment: Alignment.center,
          height: height,
          child: registerForm(width),
        );
      } else if (size.deviceScreenType == DeviceScreenType.tablet ||
          size.deviceScreenType == DeviceScreenType.desktop) {
        return Container(
          alignment: Alignment.center,
          height: height,
          child: registerForm(300),
        );
      }
      return const SizedBox();
    });
  }

  Widget registerForm(width) {
    return RegisterForm(
      width: width,
      onSubmit: (username, password) {
        
      },
    );
  }
}
