import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/utilities/dialog.dart';
import 'package:cj_app_template/widgets/button.dart';
import 'package:cj_app_template/widgets/text_field.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';

typedef OnSubmit = Function(String username, String password);

class RegisterForm extends StatefulWidget {
  final double width;
  final OnSubmit? onSubmit;
  const RegisterForm({
    Key? key,
    required this.width,
    this.onSubmit,
  }) : super(key: key);

  @override
  State<RegisterForm> createState() => _RegisterFormState();
}

class _RegisterFormState extends State<RegisterForm> {
  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController userTypeController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  void onRoleClicked() {
    List<String> items = ['ครู', 'นักเรียน', 'นักเลง', 'นักแสดง'];

    CustomDialog(
      context: context,
      title: AppLocalizations.of(context)!.pleaseSelectRole,
      onSelected: (index) {
        userTypeController.text = items[index];
      },
    ).showSelectorPicker(items, false);
  }

  void onSubmit() {
    FocusManager.instance.primaryFocus?.unfocus();
    if (isValid()) {
      if (widget.onSubmit != null) {
        widget.onSubmit!(usernameController.text, passwordController.text);
      }
    }
  }

  bool isValid() {
    if (formKey.currentState!.validate()) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          form(),
          const SizedBox(
            height: padding,
          ),
          registerButton(),
          const SizedBox(
            height: padding,
          ),
        ],
      ),
    );
  }

  Widget form() {
    return Form(
      key: formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: padding,
          ),
          // phone number
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: padding),
            child: CustomTextField(
              controller: usernameController,
              hint: AppLocalizations.of(context)!.phoneNumber,
              textInputType: TextInputType.number,
              validator: (value) => value != null && value.length < 12
                  ? AppLocalizations.of(context)!.validationPhoneNumber
                  : null,
              inputFormatters: [MaskedInputFormatter("### ### ####")],
              obscureText: false,
              width: widget.width,
              maxLine: 1,
            ),
          ),
          const SizedBox(
            height: padding,
          ),

          // password
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: padding),
            child: CustomTextField(
              controller: passwordController,
              hint: AppLocalizations.of(context)!.password,
              textInputType: TextInputType.text,
              validator: (value) => value != null && value.isEmpty
                  ? AppLocalizations.of(context)!.validationPassword
                  : null,
              inputFormatters: const [],
              obscureText: true,
              width: widget.width,
              maxLine: 1,
            ),
          ),
          const SizedBox(
            height: padding,
          ),

          // user type
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: padding),
            child: CustomTextField(
              controller: userTypeController,
              hint: AppLocalizations.of(context)!.role,
              textInputType: TextInputType.text,
              validator: (value) => value != null && value.isEmpty
                  ? AppLocalizations.of(context)!.validationRole
                  : null,
              inputFormatters: const [],
              obscureText: false,
              width: widget.width,
              maxLine: 1,
              enable: false,
              isSelector: true,
              onTab: () {
                onRoleClicked();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget registerButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: padding,
        vertical: padding,
      ),
      child: CustomButton(
        onButtonClick: () {
          onSubmit();
        },
        width: widget.width,
        buttonColor: buttonRed,
        text: AppLocalizations.of(context)!.register,
        textColor: textWhite,
        fontSize: textMeduim,
      ),
    );
  }
}
