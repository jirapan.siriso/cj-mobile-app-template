import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:cj_app_template/widgets/shimmer_item.dart';
import 'package:cj_app_template/widgets/shimmer_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:shimmer/shimmer.dart';

class ListScreen extends StatefulWidget {
  const ListScreen({Key? key}) : super(key: key);

  @override
  State<ListScreen> createState() => _ListScreenState();
}

class _ListScreenState extends State<ListScreen> {
  bool isLoading = false;
  @override
  void initState() {
    super.initState();

    if (!mounted) return;
    setState(() {
      isLoading = true;
    });
    Future.delayed(const Duration(milliseconds: 2000), () {
      if (!mounted) return;
      setState(() {
        isLoading = false;
      });
    });
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showBackButton: true,
        backButtonClick: () {
          Navigator.pop(context);
        },
        text: AppLocalizations.of(context)!.list,
        backgroundColor: backgroundDark,
      ),
      body: isLoading ? const ShimmerList() : body(),
    );
  }

  Widget body() {
    return ListView.builder(
      itemCount: 100,
      padding: const EdgeInsets.all(padding),
      itemBuilder: (BuildContext context, int index) {
        return Padding(
          padding: const EdgeInsets.only(bottom: padding),
          child: Card(
            color: buttonGray,
            child: SizedBox(
              height: 50,
              child: Center(
                child: Text(
                  '$index',
                  textScaleFactor: 1.0,
                  style: const TextStyle(
                    color: textBlack,
                    fontSize: textLarge,
                    fontWeight: FontWeight.w700,
                  ),
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  Widget shimmer() {
    return Shimmer.fromColors(
      baseColor: backgroundGray,
      highlightColor: backgroundWhite,
      enabled: isLoading,
      child: ListView.builder(
        itemBuilder: (context, index) {
          return ShimmerItem(
            index: index,
            type: shimmerType2,
          );
        },
        itemCount: 15,
      ),
    );
  }
}
