import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/screens/samples/pager/components/setting_page.dart';
import 'package:cj_app_template/screens/samples/pager/components/home_page.dart';
import 'package:cj_app_template/screens/samples/pager/components/location_page.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class PagerScreen extends StatefulWidget {
  const PagerScreen({Key? key}) : super(key: key);

  @override
  State<PagerScreen> createState() => _PagerScreenState();
}

class _PagerScreenState extends State<PagerScreen>
    with SingleTickerProviderStateMixin {
  PageController? pageController;
  TabController? tabController;
  int currentPosition = 0;

  @override
  void initState() {
    super.initState();
    pageController = PageController(
      initialPage: currentPosition,
      // viewportFraction: 0.8,
    );
    tabController = TabController(length: 3, vsync: this);
  }

  @override
  void dispose() {
    pageController!.dispose();
    tabController!.dispose();
    super.dispose();
  }

  popScreen() {
    // set screen result
    ScreenResult result = ScreenResult(
      status: false,
    );
    // pop and send result
    Navigator.pop(context, result);
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(appBarHeight * 2),
        child: AppBar(
          elevation: 0,
          backgroundColor: backgroundDark,
          title: appBar(),
          bottom: tabBar(),
        ),
      ),
      body: body(),
    );
  }

  Widget appBar() {
    return Container(
      height: appBarHeight,
      color: backgroundDark,
      alignment: Alignment.centerLeft,
      child: Text(
        AppLocalizations.of(context)!.pager,
        textScaleFactor: 1.0,
        style: const TextStyle(
          fontSize: 20,
          color: textWhite,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  TabBar tabBar() {
    return TabBar(
      controller: tabController,
      onTap: (value) {
        currentPosition = value;
        pageController!.animateToPage(
          value,
          duration: const Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      },
      tabs: const [
        Tab(
          icon: Icon(Icons.home),
        ),
        Tab(
          icon: Icon(Icons.place),
        ),
        Tab(
          icon: Icon(Icons.settings),
        ),
      ],
    );
  }

  Widget body() {
    return PageView(
      controller: pageController,
      physics: const NeverScrollableScrollPhysics(),
      // scrollDirection: Axis.horizontal,
      onPageChanged: (value) {
        currentPosition = value;
        tabController!.animateTo(
          value,
          duration: const Duration(milliseconds: 500),
          curve: Curves.ease,
        );
      },
      children: const [
        HomePage(),
        LocationPage(),
        SettingPage(),
      ],
    );
  }
}
