import 'package:cj_app_template/constants/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class SettingPage extends StatefulWidget {
  const SettingPage({Key? key}) : super(key: key);

  @override
  State<SettingPage> createState() => _SettingPageState();
}

class _SettingPageState extends State<SettingPage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        AppLocalizations.of(context)!.setting,
        textScaleFactor: 1.0,
        style: const TextStyle(
          fontSize: 30,
          color: textBlack,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
