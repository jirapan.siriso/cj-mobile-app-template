import 'package:cj_app_template/constants/color.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class LocationPage extends StatefulWidget {
  const LocationPage({Key? key}) : super(key: key);

  @override
  State<LocationPage> createState() => _LocationPageState();
}

class _LocationPageState extends State<LocationPage> {
  @override
  Widget build(BuildContext context) {
    return Center(
      child: Text(
        AppLocalizations.of(context)!.location,
        textScaleFactor: 1.0,
        style: const TextStyle(
          fontSize: 30,
          color: textBlack,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }
}
