import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/constants/interfaces.dart';
import 'package:cj_app_template/widgets/button.dart';
import 'package:cj_app_template/widgets/text_field.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:flutter_multi_formatter/formatters/masked_input_formatter.dart';

class LoginForm extends StatefulWidget {
  final double width;
  final OnSubmit? onSubmit;
  const LoginForm({
    Key? key,
    required this.width,
    this.onSubmit,
  }) : super(key: key);

  @override
  State<LoginForm> createState() => _LoginFormState();
}

class _LoginFormState extends State<LoginForm> {
  GlobalKey<FormState> formKey = GlobalKey();
  TextEditingController usernameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  void onSubmit() {
    FocusManager.instance.primaryFocus?.unfocus();
    if (isValid()) {
      if (widget.onSubmit != null) {
        widget.onSubmit!(usernameController.text, passwordController.text);
      }
    }
  }

  bool isValid() {
    if (formKey.currentState!.validate()) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: widget.width,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          form(),
          const SizedBox(
            height: padding,
          ),
          loginButton(),
          const SizedBox(
            height: padding,
          ),
        ],
      ),
    );
  }

  Widget form() {
    return Form(
      key: formKey,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: [
          const SizedBox(
            height: padding,
          ),
          // phone number
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: padding),
            child: CustomTextField(
              controller: usernameController,
              hint: AppLocalizations.of(context)!.phoneNumber,
              textInputType: TextInputType.number,
              validator: (value) => value != null && value.length < 12
                  ? AppLocalizations.of(context)!.validationPhoneNumber
                  : null,
              inputFormatters: [MaskedInputFormatter("### ### ####")],
              obscureText: false,
              width: widget.width,
              maxLine: 1,
            ),
          ),
          const SizedBox(
            height: padding,
          ),

          // password
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: padding),
            child: CustomTextField(
              controller: passwordController,
              hint: AppLocalizations.of(context)!.password,
              textInputType: TextInputType.text,
              validator: (value) => value != null && value.isEmpty
                  ? AppLocalizations.of(context)!.validationPassword
                  : null,
              inputFormatters: const [],
              obscureText: true,
              width: widget.width,
              maxLine: 1,
            ),
          ),
        ],
      ),
    );
  }

  Widget loginButton() {
    return Padding(
      padding: const EdgeInsets.symmetric(
        horizontal: padding,
        vertical: padding,
      ),
      child: CustomButton(
        onButtonClick: () {
          onSubmit();
        },
        width: widget.width,
        buttonColor: buttonRed,
        text: AppLocalizations.of(context)!.login,
        textColor: textWhite,
        fontSize: textMeduim,
      ),
    );
  }
}
