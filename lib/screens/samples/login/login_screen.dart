import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/models/respone_body.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/models/session.dart';
import 'package:cj_app_template/screens/samples/login/components/login_form.dart';
import 'package:cj_app_template/services/authen_service.dart';
import 'package:cj_app_template/utilities/dialog.dart';
import 'package:cj_app_template/utilities/shared_preferences.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';
import 'package:responsive_builder/responsive_builder.dart';

const int loginStandardMode = 1;
const int loginReAuthenMode = 2;

class LoginScreen extends StatefulWidget {
  final int mode;
  const LoginScreen({
    Key? key,
    required this.mode,
  }) : super(key: key);

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  bool isLoading = false;
  @override
  void initState() {
    super.initState();
  }

  popScreen({required bool status}) {
    // set screen result
    ScreenResult result = ScreenResult(
      status: status,
    );
    // pop and send result
    Navigator.pop(context, result);
  }

  ///
  /// service
  /// methods
  ///

  Future<ResponeBody?> login(username, password) async {
    // start loading
    isLoading = true;

    // call service
    Response? response = await AuthenService(
      context: context,
      onReLoginCompleted: (isSuccess) {
        if (isSuccess) {
          // relogin completed
          // reload data here
        }
      },
    ).login(username, password);

    // stop loading
    isLoading = false;

    if (response != null) {
      if (response.statusCode == 200) {
        // parse response
        ResponeBody responeBody = ResponeBody.fromJson(response.data);
        return responeBody;
      } else {
        // show error here
        CustomDialog(context: context).showToast(
            AppLocalizations.of(context)!.responeInternalServerError);
      }
    }

    return null;
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusManager.instance.primaryFocus?.unfocus(),
      child: WillPopScope(
        onWillPop: () async {
          if (widget.mode == loginStandardMode) {
            return true;
          } else {
            return false;
          }
        },
        child: Scaffold(
          appBar: CustomAppBar(
            showBackButton: widget.mode == loginStandardMode,
            backButtonClick: () {
              Navigator.pop(context);
            },
            text: AppLocalizations.of(context)!.login,
            backgroundColor: backgroundDark,
          ),
          body: SingleChildScrollView(
            child: body(),
          ),
        ),
      ),
    );
  }

  Widget body() {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;
    return ResponsiveBuilder(builder: (context, size) {
      if (size.deviceScreenType == DeviceScreenType.mobile ||
          size.deviceScreenType == DeviceScreenType.watch) {
        return Container(
          alignment: Alignment.center,
          height: height,
          child: loginForm(width),
        );
      } else if (size.deviceScreenType == DeviceScreenType.tablet ||
          size.deviceScreenType == DeviceScreenType.desktop) {
        return Container(
          alignment: Alignment.center,
          height: height,
          child: loginForm(300),
        );
      }
      return const SizedBox();
    });
  }

  Widget loginForm(width) {
    return LoginForm(
      width: width,
      onSubmit: (username, password) async {
        // do login service
        if (!isLoading) {
          ResponeBody? res = await login(username, password);
          if (res != null && res.session != null) {
            // session record
            await CustomSharedPreferences.instant().setSession(res.session!);

            // start to home screen here
            popScreen(status: true);
          }
        }
      },
    );
  }
}
