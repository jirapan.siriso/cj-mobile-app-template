import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/utilities/date_time_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class DrawerScreen extends StatefulWidget {
  const DrawerScreen({Key? key}) : super(key: key);

  @override
  State<DrawerScreen> createState() => _DrawerScreenState();
}

class _DrawerScreenState extends State<DrawerScreen> {
  @override
  void initState() {
    super.initState();
  }

  popScreen() {
    // set screen result
    ScreenResult result = ScreenResult(
      status: false,
    );
    // pop and send result
    Navigator.pop(context, result);
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
        preferredSize: const Size.fromHeight(appBarHeight),
        child: AppBar(
          elevation: 0,
          backgroundColor: backgroundDark,
          title: appBar(),
        ),
      ),
      drawer: Drawer(
        backgroundColor: backgroundDark,
        child: drawer(),
      ),
      body: body(),
    );
  }

  Widget appBar() {
    return Container(
      height: appBarHeight,
      color: backgroundDark,
      alignment: Alignment.centerLeft,
      child: Text(
        AppLocalizations.of(context)!.drawer,
        textScaleFactor: 1.0,
        style: const TextStyle(
          fontSize: 20,
          color: textWhite,
          fontWeight: FontWeight.w400,
        ),
      ),
    );
  }

  Widget drawer() {
    return ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
          decoration: const BoxDecoration(
            color: backgroundBlack,
          ),
          child: Center(
            child: Text(
              AppLocalizations.of(context)!.drawer,
              textScaleFactor: 1.0,
              style: const TextStyle(
                color: textWhite,
                fontWeight: fontWeight,
                fontSize: textSize,
              ),
            ),
          ),
        ),
        ListTile(
          title: Text(
            AppLocalizations.of(context)!.item + ' 1',
            textScaleFactor: 1.0,
            style: const TextStyle(
              color: textWhite,
              fontWeight: fontWeight,
              fontSize: textSize,
            ),
          ),
          onTap: () {
            // close drawer
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text(
            AppLocalizations.of(context)!.item + ' 2',
            textScaleFactor: 1.0,
            style: const TextStyle(
              color: textWhite,
              fontWeight: fontWeight,
              fontSize: textSize,
            ),
          ),
          onTap: () {
            // close drawer
            Navigator.pop(context);
          },
        ),
        ListTile(
          title: Text(
            AppLocalizations.of(context)!.back,
            textScaleFactor: 1.0,
            style: const TextStyle(
              color: textWhite,
              fontWeight: fontWeight,
              fontSize: textSize,
            ),
          ),
          onTap: () {
            // close drawer
            Navigator.pop(context);
            // pop screen
            Future.delayed(const Duration(milliseconds: 300), () {
              popScreen();
            });
          },
        ),
      ],
    );
  }

  Widget body() {
    return Container();
  }
}
