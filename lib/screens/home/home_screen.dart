import 'package:cj_app_template/configs/app_routing.dart';
import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/widgets/app_bar.dart';
import 'package:cj_app_template/widgets/button.dart';
import 'package:flutter/material.dart';
import 'package:flutter_gen/gen_l10n/app_localizations.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  gridButtonClicked() {
    AppRouting(
      context: context,
    ).startGridScreen();
  }

  listButtonClicked() {
    AppRouting(
      context: context,
    ).startListScreen();
  }

  bottomNavigatorButtonClicked() {
    AppRouting(
      context: context,
    ).startBottomNavigatorScreen();
  }

  drawerButtonClicked() {
    AppRouting(
      context: context,
    ).startDrawerScreen();
  }

  pagerButtonClicked() {
    AppRouting(
      context: context,
    ).startPagerScreen();
  }

  webButtonClicked() {
    AppRouting(
      context: context,
    ).startWebScreen();
  }

  loginButtonClicked() {
    AppRouting(
      context: context,
      onScreenResult: (ScreenResult? result) {
        if (result != null && result.status) {
          // is login completed
        }
      },
    ).startLoginScreen();
  }

  registerButtonClicked() {
    AppRouting(
      context: context,
      onScreenResult: (ScreenResult? result) {
        if (result != null && result.status) {
          // is login completed
        }
      },
    ).startRegisterScreen();
  }

  ///
  /// widget
  /// methods
  ///

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showBackButton: false,
        backButtonClick: () {},
        text: AppLocalizations.of(context)!.home,
        backgroundColor: backgroundDark,
      ),
      body: SingleChildScrollView(
        child: body(),
      ),
    );
  }

  Widget body() {
    return Column(
      children: [
        const SizedBox(
          height: padding,
        ),

        // grid screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              gridButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.grid,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // list screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              listButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.list,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // bottom navigator screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              bottomNavigatorButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.bottomMenu,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // drawer screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              drawerButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.drawer,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // pager screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              pagerButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.pager,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // web screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              webButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.web,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // login screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              loginButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.login,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        // register screen
        Padding(
          padding: const EdgeInsets.symmetric(
            horizontal: padding,
            vertical: padding / 2,
          ),
          child: CustomButton(
            onButtonClick: () {
              registerButtonClicked();
            },
            buttonColor: buttonGray,
            text: AppLocalizations.of(context)!.register,
            textColor: textBlack,
            fontSize: textMeduim,
          ),
        ),

        const SizedBox(
          height: padding,
        ),
      ],
    );
  }
}
