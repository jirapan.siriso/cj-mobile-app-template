import 'package:cj_app_template/constants/interfaces.dart';
import 'package:cj_app_template/models/screen_result.dart';
import 'package:cj_app_template/screens/samples/bottom_navigator/bottom_navigator_screen.dart';
import 'package:cj_app_template/screens/samples/drawer/drawer_screen.dart';
import 'package:cj_app_template/screens/samples/grid/grid_screen.dart';
import 'package:cj_app_template/screens/samples/list/list_screen.dart';
import 'package:cj_app_template/screens/samples/login/login_screen.dart';
import 'package:cj_app_template/screens/samples/pager/pager_screen.dart';
import 'package:cj_app_template/screens/samples/register/register_screen.dart';
import 'package:cj_app_template/screens/samples/web/web_screem.dart';
import 'package:flutter/material.dart';

class AppRouting {
  final BuildContext context;
  final OnScreenResult? onScreenResult;

  const AppRouting({
    required this.context,
    this.onScreenResult,
  });

  startLoginScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const LoginScreen(
          mode: loginStandardMode,
        ),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startGridScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const GridScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startListScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const ListScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startDrawerScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const DrawerScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startPagerScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const PagerScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startBottomNavigatorScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const BottomNavigatorScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startWebScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const WebScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }

  startRegisterScreen() async {
    ScreenResult? result = await Navigator.push(
      context,
      MaterialPageRoute(
        builder: (context) => const RegisterScreen(),
      ),
    );

    if (result != null) {
      if (onScreenResult != null) onScreenResult!(result);
    }
  }
}
