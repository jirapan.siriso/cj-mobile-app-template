const appName = "CJ template";

const prod = 1;
const dev = 2;
const applicationMode = dev;

const prodHost = "";
const devHost = "";

const notificationChannelKey = "app_template";
const notificationGroupKey = "app_template_group";
const notificationGroupName = "Normal";
const notificationChannelName = "Notify";
