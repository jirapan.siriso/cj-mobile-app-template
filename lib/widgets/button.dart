import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart' as dimens;
import 'package:cj_app_template/constants/interfaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class CustomButton extends StatelessWidget {
  final Color buttonColor;
  final OnButtonClick onButtonClick;
  final String text;
  final double? fontSize;
  final EdgeInsets? padding;
  final double? width;
  final Color textColor;
  final Gradient? gradient;
  final String? icon;
  final Color? iconColor;
  final double? iconSize;
  final double round;

  const CustomButton({
    Key? key,
    this.buttonColor = buttonGray,
    this.textColor = textBlack,
    required this.onButtonClick,
    this.text = '',
    this.padding,
    this.width,
    this.gradient,
    this.icon,
    this.iconColor = textBlack,
    this.iconSize = 20,
    this.fontSize = dimens.textMeduim,
    this.round = dimens.buttonRound,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double maxWidth = MediaQuery.of(context).size.width;
    double horizentalPadding = dimens.padding;
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: padding ??
            EdgeInsets.only(
              left: horizentalPadding,
              right: horizentalPadding,
              top: 5,
              bottom: 5,
            ),
        shadowColor: backgroundTransparent,
        primary: buttonColor,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(round),
        ),
      ),
      onPressed: () {
        onButtonClick();
      },
      child: Container(
        height: dimens.buttonHeight,
        width: width ?? maxWidth,
        alignment: Alignment.center,
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Visibility(
              visible: icon != null,
              child: SvgPicture.asset(
                icon ?? '',
                color: iconColor ?? textBlack,
                width: iconSize,
                height: iconSize,
              ),
            ),
            Visibility(
              visible: icon != null,
              child: const SizedBox(
                width: 10,
              ),
            ),
            Text(
              text,
              textScaleFactor: 1.0,
              maxLines: 1,
              style: TextStyle(
                color: textColor,
                fontSize: fontSize,
                fontWeight: FontWeight.w600,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
