import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/widgets/shimmer_item.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerGrid extends StatelessWidget {
  final int column;
  final int count;
  const ShimmerGrid({
    Key? key,
    this.column = 2,
    this.count = 20,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: backgroundGray,
      highlightColor: backgroundWhite,
      
      enabled: true,
      child: GridView.count(
        crossAxisCount: column,
        padding: const EdgeInsets.all(padding/2),
        children: List.generate(
          count,
          (index) {
            return ShimmerItem(
              index: index,
              type: shimmerType2,
              paddingItem: const EdgeInsets.all(padding/2),
            );
          },
        ),
      ),
    );
  }
}
