import 'package:cj_app_template/constants/dimen.dart' as dimens;
import 'package:flutter/material.dart';

const int shimmerType1 = 1;
const int shimmerType2 = 2;

class ShimmerItem extends StatelessWidget {
  final int index;
  final int type;
  final EdgeInsets? paddingItem;
  const ShimmerItem({
    Key? key,
    required this.index,
    this.type = shimmerType1,
    this.paddingItem,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    if (type == shimmerType1) {
      return type1();
    } else if (type == shimmerType2) {
      return type2();
    } else {
      return type1();
    }
  }

  Widget type1() {
    return Padding(
      padding: paddingItem == null
          ? const EdgeInsets.only(
              bottom: dimens.padding,
            )
          : paddingItem!,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Container(
            width: 60,
            height: 60,
            color: Colors.white,
          ),
          const Padding(
            padding: EdgeInsets.symmetric(horizontal: 8.0),
          ),
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 8.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 2.0),
                ),
                Container(
                  width: double.infinity,
                  height: 8.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 2.0),
                ),
                Container(
                  width: 40.0,
                  height: 8.0,
                  color: Colors.white,
                ),
              ],
            ),
          )
        ],
      ),
    );
  }

  Widget type2() {
    return Padding(
      padding: paddingItem == null
          ? const EdgeInsets.only(
              bottom: dimens.padding,
            )
          : paddingItem!,
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: 8.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                ),
                Container(
                  width: double.infinity,
                  height: 8.0,
                  color: Colors.white,
                ),
                const Padding(
                  padding: EdgeInsets.symmetric(vertical: 5.0),
                ),
                Container(
                  width: 40.0,
                  height: 8.0,
                  color: Colors.white,
                ),
                // const SizedBox(
                //   height: dimens.padding,
                // ),
                // const Divider(
                //   color: Colors.white,
                //   height: 2,
                // ),
              ],
            ),
          )
        ],
      ),
    );
  }
}
