import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/widgets/shimmer_item.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';

class ShimmerList extends StatelessWidget {
  const ShimmerList({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Shimmer.fromColors(
      baseColor: backgroundGray,
      highlightColor: backgroundWhite,
      enabled: true,
      child: ListView.builder(
        padding: const EdgeInsets.all(padding),
        itemBuilder: (context, index) {
          return ShimmerItem(
            index: index,
            type: shimmerType1,
          );
        },
        itemCount: 15,
      ),
    );
  }
}
