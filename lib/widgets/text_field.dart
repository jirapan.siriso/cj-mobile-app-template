import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/constants/interfaces.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/svg.dart';

class CustomTextField extends StatelessWidget {
  final double width;
  final TextEditingController controller;
  final String hint;
  final TextInputType textInputType;
  final FormFieldValidator<String> validator;
  final List<TextInputFormatter> inputFormatters;
  final int maxLine;
  final bool obscureText;
  final bool enable;
  final OnTab? onTab;
  final bool isSelector;

  const CustomTextField({
    Key? key,
    required this.controller,
    required this.hint,
    required this.textInputType,
    required this.validator,
    required this.inputFormatters,
    required this.obscureText,
    required this.width,
    required this.maxLine,
    this.enable = true,
    this.onTab,
    this.isSelector = false,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.min,
      children: [
        Text(
          hint,
          textScaleFactor: 1.0,
          style: const TextStyle(
            fontSize: textSmall,
            fontWeight: FontWeight.w400,
            color: textBlack,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        SizedBox(
          width: width,
          child: Stack(
            alignment: Alignment.topRight,
            children: [
              TextFormField(
                onTap: () {
                  if (onTab != null) {
                    onTab!();
                  }
                },
                minLines: maxLine - 1 < 1 ? 1 : maxLine - 1,
                maxLines: maxLine,
                obscureText: obscureText,
                cursorColor: textBlack,
                showCursor: enable,
                readOnly: !enable,
                style: const TextStyle(
                  fontSize: textMeduim,
                  fontWeight: FontWeight.w400,
                  color: textBlack,
                ),
                controller: controller,
                keyboardType: textInputType,
                validator: validator,
                inputFormatters: inputFormatters,
                decoration: InputDecoration(
                  contentPadding: const EdgeInsets.all(padding),
                  hintStyle: const TextStyle(
                    color: textGray,
                  ),
                  hintText: hint,
                  filled: false,
                  fillColor: backgroundWhite,
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(textFieldRound),
                    borderSide: const BorderSide(
                      color: textGray,
                      width: 2.0,
                    ),
                  ),
                  enabledBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(textFieldRound),
                    borderSide: const BorderSide(
                      color: textGray,
                      width: 2.0,
                    ),
                  ),
                  focusColor: textBlue,
                  // prefixIcon: const Padding(
                  //   padding: EdgeInsets.only(left: padding),
                  //   child: Icon(
                  //     Icons.search,
                  //     size: 16,
                  //   ),
                  // ),
                ),
              ),
              Visibility(
                visible: isSelector,
                child: Padding(
                  padding: const EdgeInsets.only(right: 10, top: 12),
                  child: SvgPicture.asset(
                    'assets/icons/down-arrow.svg',
                    width: 25,
                    height: 25,
                    color: textGray,
                  ),
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
