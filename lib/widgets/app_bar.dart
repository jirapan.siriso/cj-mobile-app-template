import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/dimen.dart';
import 'package:cj_app_template/constants/icon.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomAppBar extends StatelessWidget with PreferredSizeWidget {
  final String? text;
  final bool? showBackButton;
  final Color? backgroundColor;
  final Function? backButtonClick;
  final TabBar? tabBar;
  final Color? textColor;

  const CustomAppBar({
    this.text = '',
    this.showBackButton = true,
    this.backgroundColor = backgroundBlack,
    this.backButtonClick,
    this.tabBar,
    this.textColor,
  });

  @override
  Widget build(BuildContext context) {
    if (showBackButton != null && showBackButton!) {
      return toolbarWithBackButton();
    } else {
      return toolbarWithoutBackButton();
    }
  }

  Widget toolbarWithBackButton() {
    return AppBar(
      elevation: 0,
      backgroundColor: backgroundColor,
      toolbarHeight: appBarHeight,
      title: Container(
        height: appBarHeight,
        // color: backgroundColor,
        alignment: Alignment.centerLeft,
        child: Text(
          text ?? '',
          textScaleFactor: 1.0,
          style: TextStyle(
            fontSize: 20,
            color: textColor ?? textWhite,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
      leading: backButton(),
    );
  }

  Widget toolbarWithoutBackButton() {
    return AppBar(
      automaticallyImplyLeading: false,
      elevation: 0,
      toolbarHeight: appBarHeight,
      backgroundColor: backgroundColor,
      title: Container(
        height: appBarHeight,
        // color: backgroundColor,
        alignment: Alignment.centerLeft,
        child: Text(
          text ?? '',
          textScaleFactor: 1.0,
          style: TextStyle(
            fontSize: 20,
            color: textColor ?? textWhite,
            fontWeight: FontWeight.w400,
          ),
        ),
      ),
    );
  }

  Widget backButton() {
    return Container(
      width: 40,
      height: 40,
      margin: const EdgeInsets.only(left: padding),
      child: RawMaterialButton(
        onPressed: () {
          if (backButtonClick != null) {
            backButtonClick!();
          }
        },
        splashColor: backgroundTransparent,
        elevation: 0.0,
        fillColor: backgroundTransparent,
        shape: const CircleBorder(),
        child: SvgPicture.asset(
          leftArrowIcon,
          width: 16,
          height: 16,
          color: textColor ?? textWhite,
        ),
      ),
    );
  }

  double getAppBarHeight() {
    return tabBar == null ? appBarHeight : appBarHeight * 2;
  }

  @override
  Size get preferredSize => Size.fromHeight(getAppBarHeight());
}
