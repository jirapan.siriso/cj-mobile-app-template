import 'package:cj_app_template/constants/color.dart';
import 'package:cj_app_template/constants/interfaces.dart';
import 'package:flutter/material.dart';

class SelectorItem extends StatelessWidget {
  final String text;
  final double? width;
  final OnClicked? onClicked;
  const SelectorItem({
    Key? key,
    required this.text,
    this.width,
    this.onClicked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    double w = MediaQuery.of(context).size.width;
    return ElevatedButton(
      style: ElevatedButton.styleFrom(
        padding: const EdgeInsets.only(left: 0, right: 0, top: 0, bottom: 0),
        shadowColor: backgroundTransparent,
        primary: backgroundTransparent,
      ),
      onPressed: () {
        if (onClicked != null) {
          onClicked!();
        }
      },
      child: Column(
        children: [
          Container(
            alignment: Alignment.centerLeft,
            width: width ?? w,
            height: 50,
            child: Text(
              text,
              textScaleFactor: 1.0,
              style: const TextStyle(
                fontSize: 16,
                color: textBlack,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
          const Divider(),
        ],
      ),
    );
  }
}
